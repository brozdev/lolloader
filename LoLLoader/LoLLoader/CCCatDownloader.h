//
//  CCCatDownloader.h
//  LoLLoader
//
//  Created by Brian Rozboril on 7/8/14.
//  Copyright (c) 2014 Brian Rozboril. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CCFlickrPhoto;

typedef void (^CatDownloaderCompletionBlock)(CCFlickrPhoto *flickrPhoto, NSError *error);

@interface CCCatDownloader : NSObject

+ (instancetype)sharedDownloader;

- (void)getCatPicWithCompletionBlock:(CatDownloaderCompletionBlock)completionBlock;

@end
