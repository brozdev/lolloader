//
//  CCFlickrPhoto.m
//  LoLLoader
//
//  Created by Brian Rozboril on 7/9/14.
//  Copyright (c) 2014 Brian Rozboril. All rights reserved.
//

#import "CCFlickrPhoto.h"

static NSString * const URL_KEY         = @"url_m";
static NSString * const TITLE_KEY       = @"title";

@implementation CCFlickrPhoto

- (instancetype)initWithDictionary:(NSDictionary *)jsonDictionary {
    self = [super init];
    if(self){
        _url = [NSURL URLWithString:[jsonDictionary objectForKey:URL_KEY]];
        _title = [jsonDictionary objectForKey:TITLE_KEY];
    }
    return self;
}

@end
