//
//  CCCatDownloader.m
//  LoLLoader
//
//  Created by Brian Rozboril on 7/8/14.
//  Copyright (c) 2014 Brian Rozboril. All rights reserved.
//

#import "CCCatDownloader.h"
#import "CCFlickrPhoto.h"

static NSString * const API_KEY = @"e7c0c8d3d64180b2ac5e60108a2fd946";
static NSString * const FLICKR_SEARCH_URL = @"https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=%@&text=lolcat&safe_search=1&per_page=1&page=%i&format=json&nojsoncallback=1&content_type=1&extras=url_m";

//Flickr JSON Response Keys
static NSString * const PHOTOS_KEY = @"photos";
static NSString * const PHOTO_ARRAY_KEY = @"photo";

@interface CCCatDownloader()

@property(nonatomic, strong) NSString *urlString;

@property(nonatomic, strong) NSURLSession *session;

@end

@implementation CCCatDownloader

+ (instancetype)sharedDownloader {
    static CCCatDownloader *sharedDownloader = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedDownloader = [[self alloc] init];
    });
    return sharedDownloader;
}
- (void)getCatPicWithCompletionBlock:(CatDownloaderCompletionBlock)completionBlock {
    arc4random_stir();
    int randomPage = (arc4random() % SHRT_MAX) + 1;
    self.urlString = [NSString stringWithFormat:FLICKR_SEARCH_URL, API_KEY, randomPage];
    self.session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [self searchForCatPicWithCompletionBlock:completionBlock];
}

- (void)searchForCatPicWithCompletionBlock:(CatDownloaderCompletionBlock)completionBlock {
    __weak CCCatDownloader *safeSelf = self;
    [[self.session dataTaskWithURL:[NSURL URLWithString:_urlString] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(!error) {
            NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            NSDictionary *photosResponse = [jsonResponse objectForKey:PHOTOS_KEY];
            NSArray *photoArray = [photosResponse objectForKey:PHOTO_ARRAY_KEY];
            CCFlickrPhoto *photo = [[CCFlickrPhoto alloc] initWithDictionary:[photoArray objectAtIndex:0]];  //assuming we got a result...
            [safeSelf downloadCatPicForPhoto:photo withCompletionBlock:completionBlock];
        } else {
            [self.session invalidateAndCancel];
            dispatch_async(dispatch_get_main_queue(), ^{
                completionBlock(nil, error);
            });
        }
    }] resume];
}

- (void)downloadCatPicForPhoto:(CCFlickrPhoto*)flickrPhoto withCompletionBlock:(CatDownloaderCompletionBlock)completionBlock {
    __weak CCCatDownloader *safeSelf = self;
    NSURLRequest *request = [NSURLRequest requestWithURL:flickrPhoto.url];
    [[self.session downloadTaskWithRequest:request completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
        CCFlickrPhoto *returnPhoto = nil;
        NSError *returnError = nil;
        if(!error){
            returnPhoto = flickrPhoto;
            NSData *photoData = [NSData dataWithContentsOfURL:location];
            UIImage *photo = [UIImage imageWithData:photoData];
            flickrPhoto.photo = photo;
        } else {
            returnError = error;
        }
        [safeSelf.session invalidateAndCancel];
        dispatch_async(dispatch_get_main_queue(), ^{
            completionBlock(returnPhoto, returnError);
        });
    }] resume];
}

@end
