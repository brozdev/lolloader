//
//  CCAppDelegate.h
//  LoLLoader
//
//  Created by Brian Rozboril on 7/8/14.
//  Copyright (c) 2014 Brian Rozboril. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
