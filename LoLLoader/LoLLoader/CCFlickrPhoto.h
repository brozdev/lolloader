//
//  CCFlickrPhoto.h
//  LoLLoader
//
//  Created by Brian Rozboril on 7/9/14.
//  Copyright (c) 2014 Brian Rozboril. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCFlickrPhoto : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSURL *url;
@property (nonatomic, strong) UIImage *photo;

- (instancetype)initWithDictionary:(NSDictionary*)jsonDictionary;

@end
