//
//  CCCatPicViewController.m
//  LoLLoader
//
//  Created by Brian Rozboril on 7/8/14.
//  Copyright (c) 2014 Brian Rozboril. All rights reserved.
//

#import "CCCatPicViewController.h"
#import "CCCatDownloader.h"
#import "CCFlickrPhoto.h"

@import QuartzCore;

@interface CCCatPicViewController ()

@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, weak) IBOutlet UIButton *fetchImageButton;
@property (nonatomic, weak) IBOutlet UIImageView *catPicView;

@end

@implementation CCCatPicViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
}

- (void)displayFlickrPhoto:(CCFlickrPhoto*)flickrPhoto {
    self.catPicView.image = flickrPhoto.photo;
    
    __weak CCCatPicViewController *safeSelf = self;
    [UIView animateWithDuration:0.3f animations:^{
        safeSelf.catPicView.alpha = 1.0f;
    } completion:^(BOOL finished) {
        [safeSelf.activityIndicator stopAnimating];
        safeSelf.fetchImageButton.enabled = YES;
    }];
}

- (void)handleError {
    [self.activityIndicator stopAnimating];
    self.fetchImageButton.enabled = YES;
    [[[UIAlertView alloc] initWithTitle:@"YEOW!" message:@"Cant haz lolcat pix!  Try again!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
}

#pragma mark Button Handler
- (IBAction)fetchImageButtonPressed:(id)sender {
    self.fetchImageButton.enabled = NO;
    [self.activityIndicator startAnimating];
    
    __weak CCCatPicViewController *safeSelf = self;
    [UIView animateWithDuration:0.3f animations:^{
        safeSelf.catPicView.alpha = 0.0f;
    } completion:^(BOOL finished) {
        safeSelf.catPicView.image = nil;
        [[CCCatDownloader sharedDownloader] getCatPicWithCompletionBlock:^(CCFlickrPhoto *flickrPhoto, NSError *error) {
            if(error){
                [safeSelf handleError];
            } else {
                [safeSelf displayFlickrPhoto:flickrPhoto];
            }
        }];
    }];
}

#pragma mark - Setup UI Elements
- (void)setupUI {
    self.view.backgroundColor = [UIColor blackColor];
    [self setupActivityIndicator];
    [self setupButton];
    [self setupImageView];
}

- (void)setupActivityIndicator {
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.hidesWhenStopped = YES;
    activityIndicator.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:activityIndicator];
    
    //Add constraints to view... Centered X & Y
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:activityIndicator
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0 constant:0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:activityIndicator
                                                          attribute:NSLayoutAttributeCenterY
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeCenterY
                                                         multiplier:1.0 constant:0]];
    self.activityIndicator = activityIndicator;
}

- (void)setupButton {
    UIButton *button = [[UIButton alloc] init];
    [button addTarget:self action:@selector(fetchImageButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    button.translatesAutoresizingMaskIntoConstraints = NO;
    button.layer.borderColor = [UIColor blueColor].CGColor;
    button.layer.borderWidth = 5;
    
    [self.view addSubview:button];
    
    //Add constraints to view... standard margins to top, bottom, and sides
    NSDictionary *viewDictionary = NSDictionaryOfVariableBindings(button);
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-18-[button]-18-|" options:0 metrics:nil views:viewDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-18-[button]-18-|" options:0 metrics:nil views:viewDictionary]];
    
    self.fetchImageButton = button;
}

- (void)setupImageView {
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.translatesAutoresizingMaskIntoConstraints = NO;
    imageView.alpha = 0.0f;
    
    [self.view addSubview:imageView];
    
    NSDictionary *viewDictionary = NSDictionaryOfVariableBindings(imageView);
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-25-[imageView]-25-|" options:0 metrics:nil views:viewDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-25-[imageView]-25-|" options:0 metrics:nil views:viewDictionary]];
    
    self.catPicView = imageView;
}


@end
